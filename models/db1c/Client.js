const Sequelize = require("sequelize");
const db = require("../../db1c");

module.exports = db.sequelize.define(
    'clients',
    {
        id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
        },
        name: {
            type: Sequelize.STRING
        },
        bin: {
            type: Sequelize.STRING
        },
        contact_name: {
            type: Sequelize.STRING
        },
        telephone: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING
        },
        createdAT: {
            type: Sequelize.DATE
        },
        username: {
            type: Sequelize.STRING
        }
    },
    {
        timestamps: false
    }
);
