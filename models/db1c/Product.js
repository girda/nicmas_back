const Sequelize = require("sequelize");
const db = require("../../db1c");

module.exports = db.sequelize.define(
    'products',
    {
        id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        name: {
            type: Sequelize.STRING
        },
        code: {
            type: Sequelize.STRING
        },
        invoice_id: {
            type: Sequelize.INTEGER
        },
        createdAT: {
            type: Sequelize.DATE
        },
        username: {
            type: Sequelize.STRING
        }
    },
    {
        timestamps: false
    }
);
