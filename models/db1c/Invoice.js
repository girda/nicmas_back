const Sequelize = require("sequelize");
const db = require("../../db1c");

module.exports = db.sequelize.define(
    'invoices',
    {
        id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        number_invoice: {
            type: Sequelize.INTEGER
        },
        date_invoice: {
            type: Sequelize.INTEGER
        },
        cost_invoice: {
            type: Sequelize.INTEGER
        },
        client_id: {
            type: Sequelize.INTEGER
        },
        createdAT: {
            type: Sequelize.DATE
        },
        username: {
            type: Sequelize.STRING
        }
    },
    {
        timestamps: false
    }
);
