const Tags = require("../models/db/Tag");
const db = require("../db");
const keys = require('../config/keys');
const Product = require('../models/db/Product');
const baseRepository = require('../repository/baseRepository');

module.exports.record = (req, res) => {
    console.log(req.body);
    const productRepository = new baseRepository([Product], null, {where: {id: req.body.id}}, {username: 'success'});
    productRepository.update()
        .then(result => {
            Product.findAll({where: {username: null}})
                .then(products => {
                    res.status(200).json({products: products, message: 'success'})
                })
        })
        .catch(error => console.log(error));
};
