const jwt = require('jsonwebtoken');
const User = require('../models/db/User');
const Role = require('../models/db/Role');
const Product = require('../models/db/Product');
const keys = require('../config/keys');

module.exports.login = (req, res) => {
    const reqUser = req.body.user;
    const productId = req.body.product_id;

    User.findOne({where: {login: reqUser.login}})
        .then(user => {
            if (reqUser.password === user.password) {
                Role.findOne({where: {id: user.role}})
                    .then(role => {
                    const token = jwt.sign(user.dataValues, keys.jwt, {expiresIn: 60 * 60});
                        console.log(productId);
                    if (productId) {
                        Product.findOne({where: {code: productId}})
                            .then(product => {
                                console.log(product);
                                res.status(200).json({
                                    token: "Bearer " + token,
                                    role: role.name,
                                    product
                                });
                            })
                    } else {
                        res.status(200).json({
                            token: "Bearer " + token,
                            role: role.name
                        });
                    }
                })
            } else {
                res.status(401).json({message: 'невірний пароль'});
            }
        }).catch(err => {
        console.log(err);
        res.status(404).json({message: 'користувач відсутній'});
    })
};
