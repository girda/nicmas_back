'use strict';
const db = require('./db.js');
const db1c = require('./db1c.js');
const express = require('express');
const passport = require('passport');
const cors = require('cors');
const bodyParser = require("body-parser");
const app = express();

const Product1c = require('./models/db1c/Product');
const Client1c = require('./models/db1c/Client');
const Invoice1c = require('./models/db1c/Invoice');
const Product = require('./models/db/Product');
const Client = require('./models/db/Client');
const baseRepository = require('./repository/baseRepository');

const util = require('./helpers/util');
const keys = require('./config/keys');

const authRoutes = require('./routes/auth');
const operatorRoutes = require('./routes/operator');
const recordRoutes = require('./routes/record');

db.sequelize.authenticate()
    .then(() => {
        console.log('---BСТАНОВЛЕНО ЗЄДНАННЯ З БАЗОЮ ДАННИХ');
        db1c.sequelize.authenticate()
            .then(() => {
                console.log('---BСТАНОВЛЕНО ЗЄДНАННЯ З БАЗОЮ ДАННИХ 1c');
                const repository1C = new baseRepository([Product1c, Invoice1c, Client1c]);
                const databaseSynchronization = () => {
                    repository1C.getAll()
                        .then(result => {
                            // console.log(result);
                            return util.transformDataDB1cInDB(result, ['products', 'invoices', 'clients']);
                        })
                        .then(dataDB => {
                            // console.log(dataDB);
                            dataDB['products'].forEach(product => {
                                const productRepository = new baseRepository([Product], product, {where: {code: product.code}});
                                productRepository.create();
                            });
                            dataDB['clients'].forEach(client => {
                                const clientRepository = new baseRepository([Client], client, {where: {id: client.id}});
                                clientRepository.create();
                            });
                        });
                };
                util.setIntervalAndExecute(databaseSynchronization, keys.databaseSynchronizationTimeout)

            })
            .catch(err => {
                console.log('---ПОМИЛКА ЗЄДНАННЯ З БАЗОЮ ДАННИХ 1c:', err);
            })
    })
    .catch(err => {
        console.log('---ПОМИЛКА ЗЄДНАННЯ З БАЗОЮ ДАННИХ:', err);
    });


app.use(passport.initialize());
require('./middleware/passport')(passport);
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use("/api/auth", authRoutes);
app.use("/api/operator", operatorRoutes);
app.use("/api/record", recordRoutes);

module.exports = app;
