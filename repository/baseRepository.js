module.exports = class Repository {
    constructor(entities, item, whereCondition, updateCondition) {
        this.entities = entities;
        this.item = item;
        this.where = whereCondition || null;
        this.updateCondition = updateCondition || null;
    }

    create() {
        this.entities[0].findOne(this.where)
            .then(result => {
                if (!result) {
                    this.entities[0].create(this.item)
                        .then(result => {
                            console.log(result)
                        })
                        .catch(error => {
                            console.log(error)
                        })
                }
            })

    }

    getAll() {
        return new Promise(resolve => {
            (async () => {
                try {
                    const entitiesFindAll = [];
                    this.entities.forEach(entity => {
                        entitiesFindAll.push(entity.findAll())
                    });
                    const results = await Promise.all(entitiesFindAll);
                    resolve(results)
                } catch (err) {
                    console.error('Unable to connect to the database:', err);
                }
            })()
        })
    }

    update() {
        return new Promise(resolve => {
            console.log(this.updateCondition, this.where);
            this.entities[0].update(this.updateCondition, this.where)
                .then(result => {
                    resolve(result);
                })
        })

    }

    delete() {
        return new Promise(resolve => {
            console.log(this.updateCondition, this.where);
            this.entities[0].destroy(this.where)
                .then(result => {
                    resolve(result);
                })
        })
    }
};
