const Sequelize = require('sequelize');
const optionsDB1c = require('./config/keys').databaseOptions1c;
const db1c = {};
const sequelize = new Sequelize(
    optionsDB1c.name,
    optionsDB1c.login,
    optionsDB1c.password,
    {
        host: optionsDB1c.host,
        port: optionsDB1c.port,
        dialect: 'mysql',
        define: {
            timestamps: false
        },
        operatorsAliases: 0,
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    }
);

db1c.sequelize = sequelize;

module.exports = db1c;
