const fs = require('fs');
module.exports = class Integration1C {
    constructor() {}

    getData() {
        return new Promise(resolve => {
            console.log(' *STARTING* ');
            const contents = fs.readFileSync("./assets/1c.json");
            const jsonContent = JSON.parse(contents);
            console.log(' FILE:', jsonContent);
            console.log(' *EXIT* ');
            resolve(jsonContent);
        })
    }
};
