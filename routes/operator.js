const express = require("express");
const router = express.Router();
const passport = require('passport');
const controller = require('../controllers/operator');

router.get('/', passport.authenticate('jwt', {session: false}),  controller.operator);
router.post('/',  controller.operator); // test for staticeco

module.exports = router;
