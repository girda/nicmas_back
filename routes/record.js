const express = require("express");
const router = express.Router();
const passport = require('passport');
const controller = require('../controllers/record');

router.post('/', passport.authenticate('jwt', {session: false}), controller.record);

module.exports = router;
